<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<%@include file="/decorator/admin/header.jsp" %>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">
		<%@include file="/decorator/admin/navbar.jsp" %>
	    <!-- Main Sidebar Container -->
	    <%@include file="/decorator/admin/slidebar.jsp" %>
	    <div class="content-wrapper">
	      <!-- Content Header (Page header) -->
	      <div class="content-header">
	        <div class="container-fluid">
	          <div class="row mb-2">
	            <div class="col-sm-6">
	              <h1 class="m-0 text-dark">Danh sách tài liệu</h1>
	            </div><!-- /.col -->
	            <div class="col-sm-6">
	              <ol class="breadcrumb float-sm-right">
	                <li class="breadcrumb-item"><a href="#">pk</a></li>
	              </ol>
	            </div><!-- /.col -->
	          </div><!-- /.row -->
	        </div><!-- /.container-fluid -->
	      </div>
	      
	      <div class="card">
              <div class="card-header">
                <h3 class="card-title"></h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Mã tài liệu</th>
                    <th>Tên tài liệu</th>
                    <th>Nhà xuất bản</th>
                    <th>Hình ảnh</th>
                    <th>Số lượng</th>                  
                    <th>Hành động</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>Trident</td>
                    <td>Internet
                      Explorer 4.0
                    </td>
                    <td>Win 95+</td>
                    <td> 4</td>
                    <td>X</td>
                    <td>
                    	<a class="btn btn-block bg-gradient-success" style="font-size: 10px">
                  			Sửa
                		</a>
                    	
                    	<a href="#">Xóa</a>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
    </div>
    <%@include file="/decorator/admin/footer.jsp" %>
</body>
</html>